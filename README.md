promises-extra
====

Adds useful promise helper functions.

Supported features
===

* `retry(computation, every = 1.5, trials = 10)` executes `computation` and if it fails, reruns it `every` second(s)
until `trials`.

* `sequence(computations, concurrency = 1)` executes a list of `computations`. Also, allows specifying
`concurrency` (must be greater than or equal to 1) which runs at most `concurrency` computations in parallel.

* `throttle(computation, delay)` executes `computation` and makes sure that it takes at least `delay` seconds. If
`computation` takes longer than `delay`, additional delay will not be applied.

Change log
===

* 0.0.1 -- Initial release.
* 0.1.0 -- Fix delay bug in `retry` function. Simplify signature of `sequence` function.
* 0.1.1 -- Add context to `retry` to allow abort retrying early. 
