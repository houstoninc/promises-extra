function newRetryContext() {
   let aborted = false;

   return {
      abort: function() {
         aborted = true;
      },

      aborted: function() {
         return aborted;
      }
   };
}

module.exports = function retry(computation, every, trials) {
   every = every === undefined ? 1.5 : every;
   trials = trials === undefined ? 10 : trials;

   const ctx = newRetryContext();

   // Don't try to retry if no trials are left.
   if (trials < 0)
      return computation(ctx);

   // Do computation and then retry it if it fails.
   return computation(ctx).catch(e => new Promise(function (resolve, reject) {

         // Check if caller aborted all retires.
         if (ctx.aborted())
            return reject(e);

         // Retry computation after delay.
         setTimeout(() => resolve(), every * 1000);
      }).then(() => retry(computation, every, trials - 1))
   );
};
