module.exports = function sequence(computations, concurrency) {
   concurrency = concurrency === undefined ? 1 : concurrency;

   // Make sure that concurrency is not less than one because otherwise we don't process anything.
   if (concurrency < 1)
      throw new Error("'concurrency' must be at least 1.");

   // Do nothing if all items are already processed.
   if (computations.length === 0)
      return Promise.resolve([]);

   // Process at most 'concurrency' items in parallel.
   return Promise.all(computations.slice(0, concurrency).map(x => x()))

      // Process rest of the items.
      .then(() => sequence(computations.slice(concurrency), concurrency));
};
