module.exports = function throttle(computation, delay) {
   delay = delay === undefined ? 0 : delay;

   const t0 = new Date().getTime();

   return computation()
      .then(result => new Promise(function (resolve) {
         const t1 = new Date().getTime();

         // Resolve promise when desired delay is full. This will throttle only when computations
         // are too fast, otherwise setTimeout() will fire instantly.
         setTimeout(() => resolve(result), Math.max(delay * 1000 - (t1 - t0), 0));
      }));
};
